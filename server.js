var fs = require('fs');
var youtubedl = require('youtube-dl');
var express = require('express');
var app = express();
var server = require('http').createServer(app);
var io = require('socket.io').listen(server);
var output = 'mivydeo.mp4';

server.listen(3000);

app.get('/',function(req,res){
	res.sendFile(__dirname + '/templates/index.html');
});

app.use('/', express.static(__dirname + '/'));

var downloaded = 0;
if (fs.existsSync(output)) {
  downloaded = fs.statSync(output).size;
}

// 37 - mp4        [1080x1920]
// 46 - webm       [1080x1920]
// 22 - mp4        [720x1280]
// 45 - webm       [720x1280]
// 35 - flv        [480x854]
// 44 - webm       [480x854]
// 34 - flv        [360x640]
// 18 - mp4        [360x640]
// 43 - webm       [360x640]
// 5  - flv        [240x400]
// 17 - mp4        [144x176]

io.sockets.on('connection', function(socket){

	socket.on('url', function(data_url, format){
		var video = youtubedl(data_url,
		['--format='+format],
		{ start: downloaded, cwd: __dirname });

		video.on('info', function(info) {
		  console.log('Download started');
		  console.log('filename: ' + info._filename);

		  // info.size will be the amount to download, add
		  var total = info.size + downloaded;
		  console.log('size: ' + total);
		  socket.emit('datos_video', {
		  	title: info.title, 
		  	duration: info.duration, 
		  	format: info.format_id,
		  	size: info.size});
		});

		//video.pipe(fs.createWriteStream('mivydeo.mp4'));

		var file = fs.createWriteStream("mivydeo.mp4");
		var request = http.get("/#video", function(response) {
		  response.pipe(video.pipe(file));
		});

		// video.on('end', function(){
		// 	console.log('Descarga terminada');
		// });
	});
});
